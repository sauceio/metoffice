# Met Office

A scraper for the MetOffice DataPoint API.

## Requirements

  - On a Daily basis, scrape and store a list of all MetOffice Weather Stations (links to the API below).
  - On an Hourly basis, scrape and store Current (this hour) and Upcoming (next hour) weather for every Weather Station provided by the MetOffice (links to the API below).
  - Provide a way to get the Current and Upcoming Weather for a given Lat/Long (based on the Weather of the nearest Weather Station).
  - Provide a mechnamism for sending the Current and Upcoming Weather to a configured Module in the host application.

## Relevant Links

  - API Documentation http://www.metoffice.gov.uk/binaries/content/assets/mohippo/pdf/3/0/datapoint_api_reference.pdf
  - List of Weather Stations: http://datapoint.metoffice.gov.uk/public/data/val/wxfcs/all/xml/sitelist?key={API_KEY}
  - 3-hourly breakdown of Weather for all Weather Stations: http://datapoint.metoffice.gov.uk/public/data/val/wxfcs/all/json/all?res=3hourly&key={API_KEY}

  - The development API_KEY is `f6c484c2-58d1-4a24-bd50-af0684b08aa0`.

  - Testing Ecto database: http://www.cultivatehq.com/posts/using-ecto-2-without-phoenix-but-with-tests/

## Example Libraries (for Development)

  - An Elixir library that provides DB Migrations required by a host application: https://github.com/ueberauth/guardian_db
  - An existing Sauce scraper library: https://github.com/sauce-consultants/cineworld

## Used Libraries

  - HTTP Client: https://github.com/edgurgel/httpoison
  - JSON Parser: https://github.com/talentdeficit/exjsx
  - Time Library: https://github.com/bitwalker/timex
  - DB Migration: https://github.com/elixir-ecto/ecto and https://github.com/elixir-ecto/postgrex

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed as:

  1. Add `metoffice` to your list of dependencies in `mix.exs`:

    ```elixir
    def deps do
      [{:metoffice, "~> 0.1.0"}]
    end
    ```

  2. Ensure `metoffice` is started before your application:

    ```elixir
    def application do
      [applications: [:metoffice]]
    end
    ```
## Usage
  To updated the forecasts in the database call `Metoffice.Forecast.update_forecasts()`

  To updated the locations in the database call `Metoffice.Location.update_locations()`

  To get the nearest location with degrees (standard Android/iOS GPS data) call `Metoffice.Distance.get_closest_location_id_from_degrees(lat, long)`

## Database
  I've been running the postgres instance in a docker container with this command:
  `docker run --name metoffice-postgres -e POSTGRES_PASSWORD=metoffice -d -p 5432:5432 postgres`

  Once the database server is running we need to create the database
  `mix ecto.create`

  After the database is running you can create migrations
  `mix ecto.get.migration <migration_name>`

  To run migrations run:
  `mix ecto.migrate`

## JSON Return Location Data Overview
Urls:
  - http://datapoint.metoffice.gov.uk/public/data/val/wxfcs/all/json/sitelist?res=3hourly&key={API_KEY}

Locations:
  Location: []
    -elevation
    -id
    -latitude
    -longitude
    -name
    -region
    -unitaryAuthArea


## JSON Return Weather Data Overview
Urls: 
  - http://datapoint.metoffice.gov.uk/public/data/val/wxfcs/" <> id <> "/json/3840?res=3hourly&key={API_KEY}
  - http://datapoint.metoffice.gov.uk/public/data/val/wxfcs/all/json/3840?res=3hourly&key={API_KEY}

SiteRep:
  Wx: 
    Param: []
      -name #this is a key
      -units
      -$ #this is a text description
  DV:
    -dataDate
    -type
    Location: [] #Each location for
      -continent
      -country
      -elevation
      -i //this is an id
      -lat
      -long
      -name
      Period: [] #This is the days in the forcast
        -type
        -value #this is the date of the forcast
        -Rep: [] #Forcast for each time period in the forcast
          -$ #Time period in minutes (see Time Periods)
          -D #Match with Params from Wx
          -F #Match with Params from Wx
          -G #Match with Params from Wx
          -H #Match with Params from Wx
          -Pp #Match with Params from Wx
          -S #Match with Params from Wx
          -T #Match with Params from Wx
          -U #Match with Params from Wx
          -V #Match with Params from Wx
          -W #Match with Params from Wx

## Time Periods
| Time Period | Value | Hours |
| ----------- | ----- | ----- |
| 0 | 24/0 - 3 | 0, 1, 2 |
| 180 | 3 - 6 | 3, 4, 5 |
| 360 | 6 - 9 | 6, 7, 8 |
| 540 | 9 - 12 | 9, 10, 11 |
| 720 | 12 - 15 | 12, 13, 14 |
| 900 | 15 - 18 | 15, 16, 17 |
| 1080 | 18 - 21 | 18, 19, 20 |
| 1260 | 21 - 24/0 | 21, 22, 23 |