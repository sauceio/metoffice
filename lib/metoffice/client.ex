defmodule Metoffice.Client do

    @metoffice_hour_steps 3

    @doc """
    Return the endpoint for a weather forcast with a location id
    """
    def weather_endpoint_url(id) do
      current_time_step = calculate_the_current_time_step()

      "http://datapoint.metoffice.gov.uk/public/data/val/wxfcs/all/json/"
      <> id <> "?res=3hourly&time=#{current_time_step}&key=" <> Application.get_env(:metoffice, :api_key)
    end

    @doc """
    Return the endpoint for a weather forcast for every location
    """
    def weather_endpoint_url() do
        weather_endpoint_url("all")
    end

    @doc """
    Return the endpoint for all locations
    """
    def location_endpoint_url() do
        "http://datapoint.metoffice.gov.uk/public/data/val/wxfcs/all/json/sitelist?key=" <> Application.get_env(:metoffice, :api_key)
    end

    @doc """
    Process the request for the chosen endpoint
    """
    def process_request(endpoint) do
        HTTPoison.get!(
            endpoint,
            headers(),
            [recv_timeout: Application.get_env(:metoffice, :api_timeout)]
        ).body
    end

    @doc """
    Process the body json and return a map of the object
    """
    def process_response_body(body) do
        JSX.decode!(body)
    end

    @doc """
    Default json headers
    """
    def headers do
        %{
        "Content-Type" => "application/json",
        "Accept" => "application/json"
        }
    end

    defp calculate_the_current_time_step() do
      current_time_step_hour =
        Timex.local.hour
        |> calculate_current_time_step_hour()

      current_date =
        Timex.local
        |> Timex.format!("{YYYY}-{0M}-{0D}")

      "#{current_date}T#{current_time_step_hour}Z"
    end

    defp calculate_current_time_step_hour(current_hour) do
      in_step_number =
        current_hour
        |> Integer.floor_div(@metoffice_hour_steps)

      in_step_number * @metoffice_hour_steps
    end
end
