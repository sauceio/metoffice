defmodule Metoffice.Distance do
    @doc """
    Get closest location_id from degrees (Google Maps, iOS and Android default values)
    """
    def get_closest_location_id_from_degrees(lat, long) do
        get_closest_location_id(degrees_to_radians(lat), degrees_to_radians(long))
    end

    @doc """
    Get closest location_id from radians
    This just calls get_closest_location_id, it is mainly for ease of use.
    """
    def get_closest_location_id_from_radians(lat, long) do
        get_closest_location_id(lat, long)
    end

    @doc """
    Give a latitude and longitude in radians and get the closest locations id
    """
    def get_closest_location_id(lat, long) do
        location_distances = get_distances_for_locations(Metoffice.Schema.Location |> Metoffice.WeatherRepo.all, lat, long)

        #Distance, Id
        {_, closest} = List.first(Enum.sort(location_distances))
        closest
    end

    @doc """
    Get the distance for location starter function
    """
    def get_distances_for_locations(locations, lat, long) do
        get_distances_for_locations(locations, lat, long, [])
    end

    @doc """
    Get the distance for location, from list of locations.
    Caluclated the distance for the head and add it to the calculated list for the tail recursion
    """
    def get_distances_for_locations([head | tail], lat, long, calculated) do
        lat2 = degrees_to_radians(Decimal.to_float(head.latitude))
        lon2 = degrees_to_radians(Decimal.to_float(head.longitude))
        distance = get_distance(lat, long, lat2, lon2)
        get_distances_for_locations(tail, lat, long, calculated ++ [{distance, head.location_id}])
    end

    @doc """
    Once all of the locations are calculated return the calculated list
    """
    def get_distances_for_locations([], _, _, calculated) do
        calculated
    end

    @doc """
    Get the distance between two radian based latitudes and longitudes
    The calculation is basically:
        acos(sin(lat1) · sin(lat2) + cos(lat1) · cos(lat2) · cos(lon1 - lon2)) · R
    """
    def get_distance(lat1, lon1, lat2, lon2) do
        :math.acos(:math.sin(lat1) * :math.sin(lat2) + :math.cos(lat1) * :math.cos(lat2) * :math.cos(lon1 - lon2)) * 6371
    end

    @doc """
    Get Radians from Degrees
    """
    def degrees_to_radians(degrees) do
        (degrees / 180) * :math.pi()
    end

    @doc """
    Get Degrees from Radians
    """
    def radians_to_degrees(radians) do
        (radians * 180) / :math.pi()
    end
end
