defmodule Metoffice.Time do

    @doc """
    Get current forecast periods
    """
    def current_forecast_periods() do
        hour = Timex.local.hour
        forecast_periods_for_hour(hour)
    end

    @doc """
    Get forecast period for hour 0
    """
    def forecast_periods_for_hour(0) do
        [0, 0]
    end

    @doc """
    Get forecast period for hour 1
    """
    def forecast_periods_for_hour(1) do
        [0, 0]
    end

    @doc """
    Get forecast period for hour 2
    """
    def forecast_periods_for_hour(2) do
        [0, 180]
    end

    @doc """
    Get forecast period for hour 3
    """
    def forecast_periods_for_hour(3) do
        [180, 180]
    end

    @doc """
    Get forecast period for hour 4
    """
    def forecast_periods_for_hour(4) do
        [180, 180]
    end

    @doc """
    Get forecast period for hour 5
    """
    def forecast_periods_for_hour(5) do
        [180, 360]
    end

    @doc """
    Get forecast period for hour 6
    """
    def forecast_periods_for_hour(6) do
        [360, 360]
    end

    @doc """
    Get forecast period for hour 7
    """
    def forecast_periods_for_hour(7) do
        [360, 360]
    end

    @doc """
    Get forecast period for hour 8
    """
    def forecast_periods_for_hour(8) do
        [360, 540]
    end

    @doc """
    Get forecast period for hour 9
    """
    def forecast_periods_for_hour(9) do
        [540, 540]
    end

    @doc """
    Get forecast period for hour 10
    """
    def forecast_periods_for_hour(10) do
        [540, 540]
    end

    @doc """
    Get forecast period for hour 11
    """
    def forecast_periods_for_hour(11) do
        [540, 720]
    end    

    @doc """
    Get forecast period for hour 12
    """
    def forecast_periods_for_hour(12) do
        [720, 720]
    end

    @doc """
    Get forecast period for hour 13
    """
    def forecast_periods_for_hour(13) do
        [720, 720]
    end

    @doc """
    Get forecast period for hour 14
    """
    def forecast_periods_for_hour(14) do
        [720, 900]
    end

    @doc """
    Get forecast period for hour 15
    """
    def forecast_periods_for_hour(15) do
        [900, 900]
    end

    @doc """
    Get forecast period for hour 16
    """
    def forecast_periods_for_hour(16) do
        [900, 900]
    end

    @doc """
    Get forecast period for hour 17
    """
    def forecast_periods_for_hour(17) do
        [900, 1080]
    end

    @doc """
    Get forecast period for hour 18
    """
    def forecast_periods_for_hour(18) do
        [1080, 1080]
    end

    @doc """
    Get forecast period for hour 19
    """
    def forecast_periods_for_hour(19) do
        [1080, 1080]
    end

    @doc """
    Get forecast period for hour 20
    """
    def forecast_periods_for_hour(20) do
        [1080, 1260]
    end

    @doc """
    Get forecast period for hour 21
    """
    def forecast_periods_for_hour(21) do
        [1260, 1260]
    end

    @doc """
    Get forecast period for hour 22
    """
    def forecast_periods_for_hour(22) do
        [1260, 1260]
    end

    @doc """
    Get forecast period for hour 23
    """
    def forecast_periods_for_hour(23) do
        [1260, 1440]
    end
    @doc """
    Get forecast period for hour fall through
    """
    def forecast_periods_for_hour(_) do
        []
    end
end