defmodule Metoffice.Location do
    require Ecto.Query

    @doc """
    Get all locations from endpoint as a map
    """
    def get() do
        url = Metoffice.Client.location_endpoint_url()
        body = Metoffice.Client.process_request(url)
        Metoffice.Client.process_response_body(body)
    end

    @doc """
    Update all locations
    """
    def update_locations() do
        locations = get()["Locations"]["Location"]

        Enum.each(locations, fn(loc) -> insert_or_update(loc) end)
    end

    def insert_or_update(location) do
        loc = schema_location_from_json(location)

        #find or update
        old_loc = Metoffice.Schema.Location |> Metoffice.WeatherRepo.get_by(location_id: loc.location_id)

        #if the location does not exist, crate one else update the current one
        cond do
            old_loc == nil ->
                insert_location(loc)
            old_loc != nil ->
                update_location(loc, old_loc)
        end
    end

    @doc """
    Insert Location into database
    """
    def insert_location(location) do
        Metoffice.WeatherRepo.insert(location)
    end

    @doc """
    Update Location in database
    """
    def update_location(updated_location, location) do
        changeset = Metoffice.Schema.Location.changeset(location, %{
            elevation: updated_location.elevation,
            latitude: updated_location.latitude,
            longitude: updated_location.longitude,
            name: updated_location.name,
            region: updated_location.region,
            unitary_auth_area: updated_location.unitary_auth_area,
            lastupdated: updated_location.lastupdated,
            obs_source: updated_location.obs_source})
        Metoffice.WeatherRepo.update(changeset)
    end

    @doc """
    Get Schema object from Json Map, -Default Case
    """
    def schema_location_from_json(%{"elevation" => elevation, "id" => id, "latitude" => latitude, "longitude"=> longitude, "name"=> name, "region"=> region, "unitaryAuthArea"=> unitAuthArea }) do
        {elev, ""} = Float.parse(elevation)
        {lat, ""} = Float.parse(latitude)
        {long, ""} = Float.parse(longitude)
        {loc_id, ""} = Integer.parse(id)
        %Metoffice.Schema.Location{location_id: loc_id, elevation: elev, latitude: lat, longitude: long, name: name, region: region, unitary_auth_area: unitAuthArea, lastupdated: Timex.to_unix(Timex.local)}
    end

    @doc """
    Get Schema object from Json Map, -No Elevation
    """
    def schema_location_from_json(%{"id" => id, "latitude" => latitude, "longitude"=> longitude, "name"=> name, "region"=> region, "unitaryAuthArea"=> unitAuthArea }) do
        {lat, ""} = Float.parse(latitude)
        {long, ""} = Float.parse(longitude)
        {loc_id, ""} = Integer.parse(id)
        %Metoffice.Schema.Location{location_id: loc_id, latitude: lat, longitude: long, name: name, region: region, unitary_auth_area: unitAuthArea, lastupdated: Timex.to_unix(Timex.local)}
    end

    @doc """
    Get Schema object from Json Map, -No Unitary Auth Area
    """
    def schema_location_from_json(%{"elevation" => elevation, "id" => id, "latitude" => latitude, "longitude"=> longitude, "name"=> name, "region"=> region }) do
        {elev, ""} = Float.parse(elevation)
        {lat, ""} = Float.parse(latitude)
        {long, ""} = Float.parse(longitude)
        {loc_id, ""} = Integer.parse(id)
        %Metoffice.Schema.Location{location_id: loc_id, elevation: elev, latitude: lat, longitude: long, name: name, region: region, lastupdated: Timex.to_unix(Timex.local)}
    end

    @doc """
    Get Schema object from Json Map, -OBS Value, no Unitary Auth Area, no Region
    """
    def schema_location_from_json(%{"elevation" => elevation, "id" => id, "latitude" => latitude, "longitude"=> longitude, "name"=> name, "obsSource"=> obsSource }) do
        {elev, ""} = Float.parse(elevation)
        {lat, ""} = Float.parse(latitude)
        {long, ""} = Float.parse(longitude)
        {loc_id, ""} = Integer.parse(id)
       %Metoffice.Schema.Location{location_id: loc_id, elevation: elev, latitude: lat, longitude: long, name: name, lastupdated: Timex.to_unix(Timex.local), obs_source: obsSource}
    end

    @doc """
    Get Schema object from Json Map,  no OBS Value, no Unitary Auth Area, no Region
    """
    def schema_location_from_json(%{"elevation" => elevation, "id" => id, "latitude" => latitude, "longitude"=> longitude, "name"=> name}) do
        {elev, ""} = Float.parse(elevation)
        {lat, ""} = Float.parse(latitude)
        {long, ""} = Float.parse(longitude)
        {loc_id, ""} = Integer.parse(id)
       %Metoffice.Schema.Location{location_id: loc_id, elevation: elev, latitude: lat, longitude: long, name: name, lastupdated: Timex.to_unix(Timex.local)}
    end

    @doc """
    Catch all for incase the record changes
    """
    def schema_location_from_json(response) do
      IO.puts "No match for object: " <> response
    end

end
