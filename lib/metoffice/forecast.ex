defmodule Metoffice.Forecast do
    @doc """
    Get 5 day forecast for a specific location
    """
    def get(id) do
        url = Metoffice.Client.weather_endpoint_url(id)
        body = Metoffice.Client.process_request(url)
        Metoffice.Client.process_response_body(body)
    end

    @doc """
    Get 5 day forecast for every location
    """
    def get() do
        url = Metoffice.Client.weather_endpoint_url()
        body = Metoffice.Client.process_request(url)
        Metoffice.Client.process_response_body(body)
    end

    @doc """
    Update all forecasts
    """
    def update_forecasts() do
        [current_weather_period, next_weather_period] = Metoffice.Time.current_forecast_periods()
        locations = get()["SiteRep"]["DV"]["Location"]
        Enum.each(locations, fn(loc) -> update_forecasts_for_location(loc, current_weather_period, next_weather_period) end)
    end

    @doc """
    Update forecast for location,
    This is a special use case where the first forecast of tomorrow is required for the next hours forecast
    """
    def update_forecasts_for_location(location_map, current_period, 1440) do
      current_forecast_rep = location_map["Period"]
      current_forecast = get_forecast_for_period_from_rep(current_forecast_rep["Rep"], current_period)
      store_forecast(:current, location_map["i"], current_forecast)
    end

    @doc """
    Update forecast for location
    """
    def update_forecasts_for_location(location_map, current_period, next_period) do
        current_forecast_rep = location_map["Period"]
        current_forecast = current_forecast_rep["Rep"]

        store_forecast(:current, location_map["i"], current_forecast)
    end

    @doc """
    Get the rep for the current day
    """
    def get_rep_for_today([head | tail]) do
        {:ok, local_time} = Timex.format(Timex.local, "%Y-%m-%dZ", :strftime)

        cond do
            head["value"] == local_time ->
                head
            true ->
                get_rep_for_today(tail)
        end
    end

    @doc """
    Get the period forecast from the rep
    """
    def get_forecast_for_period_from_rep([head | tail], period) do
        {per, ""} = Integer.parse(head["$"])

        cond do
            per == period ->
                head
            true ->
                get_forecast_for_period_from_rep(tail, period)
        end
    end

    @doc """
    If this contains an empty list, something went wrong getiing the reps from the period
    """
    def get_forecast_for_period_from_rep([], _) do
        :error
    end

    @doc """
    Insert or update the current forecast
    """
    def store_forecast(:current, location_id, forecast) do
        old_for = Metoffice.Schema.CurrentForecast |> Metoffice.WeatherRepo.get_by(location_id: location_id)
        forc = current_forecast_schema_from_json(location_id, forecast)

        cond do
            old_for == nil ->
                insert_forecast(forc)
            old_for != nil ->
                update_forecast(:current, old_for, forc)
        end
    end

    @doc """
    Insert or update the next hours forecast
    """
    def store_forecast(:next, location_id, forecast) do
        old_for = Metoffice.Schema.NextForecast |> Metoffice.WeatherRepo.get_by(location_id: location_id)
        next_for = next_forecast_schema_from_json(location_id, forecast)

        cond do
            old_for == nil ->
                insert_forecast(next_for)
            old_for != nil ->
                update_forecast(:next, old_for, next_for)
        end
    end

    @doc """
    Inserts a new forecast through the repo for both current and next forecasts
    """
    def insert_forecast(forecast) do
           Metoffice.WeatherRepo.insert(forecast)
    end

    @doc """
    Update the current forecast
    """
    def update_forecast(:current, forecast, updated_forecast) do
        changeset = Metoffice.Schema.CurrentForecast.changeset(forecast, %{
            lastupdated: updated_forecast.lastupdated,
            forcasttime: updated_forecast.forcasttime,
            feelslike: updated_forecast.feelslike,
            windgust: updated_forecast.windgust,
            relativehumidity: updated_forecast.relativehumidity,
            temperature: updated_forecast.temperature,
            visibility: updated_forecast.visibility,
            winddirection: updated_forecast.winddirection,
            windspeed: updated_forecast.windspeed,
            uvindex: updated_forecast.uvindex,
            weathertype: updated_forecast.weathertype,
            precipitation: updated_forecast.precipitation
        })

        Metoffice.WeatherRepo.update(changeset)
    end

    @doc """
    Update the next forecast
    """
    def update_forecast(:next, forecast, updated_forecast) do
        changeset = Metoffice.Schema.NextForecast.changeset(forecast, %{
            lastupdated: updated_forecast.lastupdated,
            forcasttime: updated_forecast.forcasttime,
            feelslike: updated_forecast.feelslike,
            windgust: updated_forecast.windgust,
            relativehumidity: updated_forecast.relativehumidity,
            temperature: updated_forecast.temperature,
            visibility: updated_forecast.visibility,
            winddirection: updated_forecast.winddirection,
            windspeed: updated_forecast.windspeed,
            uvindex: updated_forecast.uvindex,
            weathertype: updated_forecast.weathertype,
            precipitation: updated_forecast.precipitation
        })

        Metoffice.WeatherRepo.update(changeset)
    end

    @doc """
    Get current forecast schema from json map
    """
    def current_forecast_schema_from_json(location_id, forecast) do
        {loc_id, ""} = Integer.parse(location_id)
        {forecast_time, ""} = Integer.parse(forecast["$"])
        {feels_like, ""} = Integer.parse(forecast["F"])
        {wind_gust, ""} = Integer.parse(forecast["G"])
        {humidity, ""} =  Integer.parse(forecast["H"])
        {temp, ""} = Integer.parse(forecast["T"])
        {wind_speed, ""} = Integer.parse(forecast["S"])
        {uv, ""}  = Integer.parse(forecast["U"])
        {type, ""}  = Integer.parse(forecast["W"])
        {prec, ""} = Integer.parse(forecast["Pp"])

        %Metoffice.Schema.CurrentForecast{
            location_id: loc_id,
            lastupdated: Timex.to_unix(Timex.local),
            forcasttime: forecast_time,
            feelslike: feels_like,
            windgust: wind_gust,
            relativehumidity: humidity,
            temperature: temp,
            visibility: forecast["V"],
            winddirection: forecast["D"],
            windspeed: wind_speed,
            uvindex: uv,
            weathertype: type,
            precipitation: prec
        }
    end

    @doc """
    Get next forecast schema from json map
    """
    def next_forecast_schema_from_json(location_id, forecast) do
        {loc_id, ""} = Integer.parse(location_id)
        {forecast_time, ""} = Integer.parse(forecast["$"])
        {feels_like, ""} = Integer.parse(forecast["F"])
        {wind_gust, ""} = Integer.parse(forecast["G"])
        {humidity, ""} =  Integer.parse(forecast["H"])
        {temp, ""} = Integer.parse(forecast["T"])
        {wind_speed, ""} = Integer.parse(forecast["S"])
        {uv, ""}  = Integer.parse(forecast["U"])
        {type, ""}  = Integer.parse(forecast["W"])
        {prec, ""} = Integer.parse(forecast["Pp"])

        %Metoffice.Schema.NextForecast{
            location_id: loc_id,
            lastupdated: Timex.to_unix(Timex.local),
            forcasttime: forecast_time,
            feelslike: feels_like,
            windgust: wind_gust,
            relativehumidity: humidity,
            temperature: temp,
            visibility: forecast["V"],
            winddirection: forecast["D"],
            windspeed: wind_speed,
            uvindex: uv,
            weathertype: type,
            precipitation: prec
        }
    end
end
