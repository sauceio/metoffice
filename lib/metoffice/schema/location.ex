defmodule Metoffice.Schema.Location do
    use Ecto.Schema

    schema "locations" do
        field :location_id, :integer
        field :elevation, :decimal
        field :latitude, :decimal
        field :longitude, :decimal
        field :name, :string
        field :region, :string
        field :unitary_auth_area, :string
        field :lastupdated, :integer
        field :obs_source, :string
    end

    @doc """
    Get changeset for location update
    """
    def changeset(location, params \\ %{}) do
        location
        |> Ecto.Changeset.cast(params, [:elevation, :latitude, :longitude, :name, :region, :unitary_auth_area, :lastupdated, :obs_source])
    end
end
