defmodule Metoffice.Schema.CurrentForecast do
    use Ecto.Schema

    schema "current_forecast" do
        field :location_id, :integer
        field :lastupdated, :integer
        field :forcasttime, :integer
        field :feelslike, :integer
        field :windgust, :integer
        field :relativehumidity, :integer
        field :temperature, :integer
        field :visibility, :string
        field :winddirection, :string
        field :windspeed, :integer
        field :uvindex, :integer
        field :weathertype, :integer
        field :precipitation, :integer
    end

    @doc """
    Get changeset for current_forcast update
    """
    def changeset(current_forecast, params \\ %{}) do
        current_forecast
        |> Ecto.Changeset.cast(params, [
            :lastupdated, 
            :forcasttime, 
            :feelslike, 
            :windgust, 
            :relativehumidity, 
            :temperature, 
            :visibility, 
            :winddirection, 
            :windspeed, 
            :uvindex, 
            :weathertype, 
            :precipitation
        ])
    end
end