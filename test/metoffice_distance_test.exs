defmodule MetofficeDistanceTest do
    use ExUnit.Case
    doctest Metoffice

    test "Degrees to Radions is correct" do
        assert Metoffice.Distance.degrees_to_radians(0) == 0
        assert Metoffice.Distance.degrees_to_radians(90) == (:math.pi() / 2)
        assert Metoffice.Distance.degrees_to_radians(180) == :math.pi()
        assert Metoffice.Distance.degrees_to_radians(270) == ((:math.pi() / 2) * 3)
        assert Metoffice.Distance.degrees_to_radians(360) == (2 * :math.pi())
    end

    test "Radions to Degrees is correct" do
        assert Metoffice.Distance.radians_to_degrees(0) == 0
        assert Metoffice.Distance.radians_to_degrees(:math.pi() / 2) == 90
        assert Metoffice.Distance.radians_to_degrees(:math.pi()) == 180
        assert Metoffice.Distance.radians_to_degrees((:math.pi() / 2) * 3) == 270
        assert Metoffice.Distance.radians_to_degrees(2 * :math.pi()) == 360
    end

    test "Get distance is correct" do
        assert Metoffice.Distance.get_distance(1, 1, 2, 2) == 5552.4617873415145
    end

    test "Test the distances are iterated through correctly" do
        test_location_1 = %Metoffice.Schema.Location{
            location_id: 1, 
            latitude: Decimal.new(Metoffice.Distance.radians_to_degrees(1)), 
            longitude: Decimal.new(Metoffice.Distance.radians_to_degrees(1))
        }

        test_location_2 = %Metoffice.Schema.Location{
            location_id: 2, 
            latitude: Decimal.new(Metoffice.Distance.radians_to_degrees(2)), 
            longitude: Decimal.new(Metoffice.Distance.radians_to_degrees(2))
        }

        test_location_3 = %Metoffice.Schema.Location{
            location_id: 3, 
            latitude: Decimal.new(Metoffice.Distance.radians_to_degrees(3)), 
            longitude: Decimal.new(Metoffice.Distance.radians_to_degrees(3))
        }

        test_location_4 = %Metoffice.Schema.Location{
            location_id: 4, 
            latitude: Decimal.new(Metoffice.Distance.radians_to_degrees(4)), 
            longitude: Decimal.new(Metoffice.Distance.radians_to_degrees(4))
        }

        test_locations = [test_location_1, test_location_2, test_location_3, test_location_4]
        distances = Metoffice.Distance.get_distances_for_locations(test_locations, 1, 1)
        {distance, closest_id} = List.first(Enum.sort(distances))
        
        assert distance == 0.0
        assert closest_id == 1
    end
end