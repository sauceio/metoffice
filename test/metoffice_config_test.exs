defmodule MetofficeConfigTest do
    use ExUnit.Case
    doctest Metoffice

    test "API Key is set correctly on test" do
        assert Application.get_env(:metoffice, :api_key) == "{API_KEY}"
    end
end