defmodule MetofficeTimeTest do
    use ExUnit.Case
    doctest Metoffice

    test "hours return correct forcast periods" do
        assert Metoffice.Time.forecast_periods_for_hour(0) == [0, 0]
        assert Metoffice.Time.forecast_periods_for_hour(1) == [0, 0]
        assert Metoffice.Time.forecast_periods_for_hour(2) == [0, 180]
        assert Metoffice.Time.forecast_periods_for_hour(3) == [180, 180]
        assert Metoffice.Time.forecast_periods_for_hour(4) == [180, 180]
        assert Metoffice.Time.forecast_periods_for_hour(5) == [180, 360]
        assert Metoffice.Time.forecast_periods_for_hour(6) == [360, 360]
        assert Metoffice.Time.forecast_periods_for_hour(7) == [360, 360]
        assert Metoffice.Time.forecast_periods_for_hour(8) == [360, 540]
        assert Metoffice.Time.forecast_periods_for_hour(9) == [540, 540]
        assert Metoffice.Time.forecast_periods_for_hour(10) == [540, 540]
        assert Metoffice.Time.forecast_periods_for_hour(11) == [540, 720]
        assert Metoffice.Time.forecast_periods_for_hour(12) == [720, 720]
        assert Metoffice.Time.forecast_periods_for_hour(13) == [720, 720]
        assert Metoffice.Time.forecast_periods_for_hour(14) == [720, 900]
        assert Metoffice.Time.forecast_periods_for_hour(15) == [900, 900]
        assert Metoffice.Time.forecast_periods_for_hour(16) == [900, 900]
        assert Metoffice.Time.forecast_periods_for_hour(17) == [900, 1080]
        assert Metoffice.Time.forecast_periods_for_hour(18) == [1080, 1080]
        assert Metoffice.Time.forecast_periods_for_hour(19) == [1080, 1080]
        assert Metoffice.Time.forecast_periods_for_hour(20) == [1080, 1260]
        assert Metoffice.Time.forecast_periods_for_hour(21) == [1260, 1260]
        assert Metoffice.Time.forecast_periods_for_hour(22) == [1260, 1260]
        assert Metoffice.Time.forecast_periods_for_hour(23) == [1260, 1440]

        assert Metoffice.Time.forecast_periods_for_hour(-1) == []
        assert Metoffice.Time.forecast_periods_for_hour(24) == []
    end
end