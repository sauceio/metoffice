defmodule MetofficeClientTest do
    use ExUnit.Case
    doctest Metoffice

    test "Empty weather_endpoint_url returns correct URL" do
        assert "http://datapoint.metoffice.gov.uk/public/data/val/wxfcs/all/json/all?res=3hourly&key={API_KEY}" == Metoffice.Client.weather_endpoint_url()
    end

    test "weather_endpoint_url with id returns correct URL" do
        assert "http://datapoint.metoffice.gov.uk/public/data/val/wxfcs/all/json/3840?res=3hourly&key={API_KEY}" == Metoffice.Client.weather_endpoint_url("3840")
    end

    test "location_endpoint_url returns correct URL" do
        assert "http://datapoint.metoffice.gov.uk/public/data/val/wxfcs/all/json/sitelist?key={API_KEY}" == Metoffice.Client.location_endpoint_url()
    end
end