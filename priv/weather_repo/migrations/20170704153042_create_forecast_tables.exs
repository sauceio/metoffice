defmodule Metoffice.WeatherRepo.Migrations.CreateForcastTables do
  use Ecto.Migration

  def change do
    create table(:current_forecast) do
      add :location_id, :integer
      add :lastupdated, :integer
      add :forcasttime, :integer
      add :feelslike, :integer
      add :windgust, :integer
      add :relativehumidity, :integer
      add :temperature, :integer
      add :visibility, :string
      add :winddirection, :string
      add :windspeed, :integer
      add :uvindex, :integer
      add :weathertype, :integer
      add :precipitation, :integer
    end

    create table(:next_forecast) do
      add :location_id, :integer
      add :lastupdated, :integer
      add :forcasttime, :integer
      add :feelslike, :integer
      add :windgust, :integer
      add :relativehumidity, :integer
      add :temperature, :integer
      add :visibility, :string
      add :winddirection, :string
      add :windspeed, :integer
      add :uvindex, :integer
      add :weathertype, :integer
      add :precipitation, :integer
    end
  end
end
