defmodule Metoffice.WeatherRepo.Migrations.CreateLocations do
  use Ecto.Migration

  def change do
    create table(:locations) do
        add :location_id, :integer
        add :elevation, :decimal
        add :latitude, :decimal
        add :longitude, :decimal
        add :name, :string
        add :region, :string
        add :unitary_auth_area, :string
      end
  end
end
