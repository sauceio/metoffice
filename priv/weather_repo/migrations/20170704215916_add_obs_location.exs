defmodule Metoffice.WeatherRepo.Migrations.AddObsLocation do
  use Ecto.Migration

  def change do
    alter table(:locations) do
      add :obs_source, :string
    end
  end
end
