defmodule Metoffice.WeatherRepo.Migrations.AddLastUpdatedToLocations do
  use Ecto.Migration

  def change do
    alter table(:locations) do
      add :lastupdated, :integer
    end
  end
end
