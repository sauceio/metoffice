# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
use Mix.Config

config :metoffice, Metoffice.WeatherRepo,
  adapter: Ecto.Adapters.Postgres,
  database: "metoffice_weather_repo",
  username: "user",
  password: "pass",
  hostname: "localhost"

config :metoffice, ecto_repos: [Metoffice.WeatherRepo]

config :metoffice,
    api_key: System.get_env("METOFFICE_API_KEY"),
    api_timeout: 10000
    
config :quantum, :metoffice,
  cron: [
  ]

import_config "#{Mix.env}.exs"