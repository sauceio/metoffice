use Mix.Config

config :metoffice, Metoffice.WeatherRepo,
  adapter: Ecto.Adapters.Postgres,
  database: "metoffice_weather_repo",
  username: "postgres",
  password: "metoffice",
  hostname: "localhost",
  port: "5432",
  pool: Ecto.Adapters.SQL.Sandbox

config :metoffice,
    api_key: "{API_KEY}",
    api_timeout: 10000

config :quantum, :metoffice,
  cron: [
  ]