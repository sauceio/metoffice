use Mix.Config

config :metoffice, Metoffice.WeatherRepo,
  adapter: Ecto.Adapters.Postgres,
  database: "metoffice_weather_repo",
  username: "postgres",
  password: "metoffice",
  hostname: "localhost",
  port: "5432"

config :metoffice,
    api_key: System.get_env("METOFFICE_API_KEY"),
    api_timeout: 10000

config :quantum, :metoffice,
  cron: [
  ]